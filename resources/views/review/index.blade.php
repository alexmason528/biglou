@extends('layout.master')
@section('pjax-content')

<div class='row item final section'>
	<div class='row title'>
		<div class='col-md-6 col-md-offset-3'>
			<h1>Review your application</h1>
		</div>
	</div>
	<div class='content'>
		<div>
			<div class='review-item full-name'>
				<div class='review-title'>Full Name</div>
				<div class='review-value' name='app_name'></div>
			</div>
			<div class='review-item policy-owners-name'>
				<div class='review-title'>Policy Owners Name</div>
				<div class='review-value' name='app_policyowner_name'></div>
			</div>
		</div>
		<div>
			<div class='review-item birth-state'>
				<div class='review-title'>Birth State</div>
				<div class='review-value' name='app_birth_state'></div>
			</div>
			<div class='review-item birth-state'>
				<div class='review-title'>Birth State</div>
				<div class='review-value' name='app_birth_state'></div>
			</div>
		</div>
	</div>
	<div class='button-wrapper'>
		<a class='btn btn-primary back'>
			<span>Back</span>
		</a>
		<a class='btn btn-primary submit-application'>
			<span>Submit Application</span>
			<span class='glyphicon glyphicon-ok'></span>
		</a>
	</div>
	<div class='show-step'>
		FINAL STEP!
	</div>
</div>

@endsection