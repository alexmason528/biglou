@extends('layout.master')
@section('pjax-content')
<div class='row item section'>
	<div class='row title'>
		<div class='col-md-6 col-md-offset-3'>
			<h1>1. What is your full legal name?*</h1>
		</div>
	</div>
	<div class='content'>
		<div>
			<input type='text' placeholder='Type something' class='required' name='app_name' />
		</div>
	</div>
	<div class='button-wrapper'>
		<a class='btn btn-primary proceed'>
			Press Enter or Click Here
			<span class='glyphicon glyphicon-ok'></span>
		</a>
	</div>
	<div class='show-step'>
		1/23 STEPS
	</div>
</div>
@endsection
